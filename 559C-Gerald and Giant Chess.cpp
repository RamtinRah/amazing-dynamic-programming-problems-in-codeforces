/// link to problem : https://codeforces.com/problemset/problem/559/C




#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back
using namespace std;
typedef long long ll;
typedef pair<int,int> pii;
typedef long double ld;
typedef map<string,int>::iterator mapit;
typedef set<pii>::iterator setit;
const int maxn=2e5+6;
const ll MOD=1e9+7;
const double eps=1e-9;
const ld pi=3.1416;
pair<ll,ll> a[maxn];
ll dp[maxn],fact[maxn];
ll ans;
void pre()
{
    fact[0]=1;
    for(ll i=1;i<maxn;i++)
    {
        fact[i]=fact[i-1]*i;
        fact[i]%=MOD;
    }
}
ll pw(ll a , ll b)
{
    if(b==0)
        return 1;
    ll res= pw(a , b / 2);
    res = res * res;
    res %= MOD;
    if(b%2==0)
        return res;
    else
    {
        res *= a;
        res %= MOD;
        return res;
    }
}
ll C(ll a , ll b)
{
    ll c = fact[a] * fact[b-a];
    c %= MOD;
    ll res = fact[b] * pw(c,MOD-2);
    res %= MOD;
    return res;
}
int main()
{
    ios_base::sync_with_stdio(0);cin.tie(0);
    pre();
    int n,m,k; cin>>n>>m>>k;
    for(int i=0;i<k;i++)
    {
        cin>>a[i].first>>a[i].second;
        a[i].first--;
        a[i].second--;
    }
    sort(a,a+k);
    a[k].first = n-1 ; a[k].second=m-1 ;
    //cout<<C(1,2)<<" "<<C(2,3)<<" "<<C(2,5);
    for(int i=0;i<=k;i++)
    {
        dp[i]=C(a[i].first,a[i].first+a[i].second);
        for(int j=0;j<i;j++)
        {
            if(a[j].first<=a[i].first && a[j].second<=a[i].second)
            {
                ll minu=dp[j] * C(a[i].first-a[j].first , a[i].first-a[j].first+a[i].second-a[j].second);
                minu%=MOD;
                dp[i]-=minu;
            }
        }
        while(dp[i]<0)
            dp[i]+=MOD;
        //cout<<dp[i]<<" ";
    }
    cout<<dp[k];
    return 0;
}
