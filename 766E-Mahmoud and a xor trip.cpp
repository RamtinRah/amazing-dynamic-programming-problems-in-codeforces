
///link to problem : https://codeforces.com/problemset/problem/766/E

#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back
using namespace std;
typedef long long ll;
typedef pair<int,int> pii;
typedef long double ld;
typedef map<pii,set<pii> >::iterator mapit;
typedef multiset<ll>::iterator setit;
const int maxn = 1e5 + 5;
const int maxlog = 25;
const ll mod = 1e9 + 7;
const int sq = 720;
const int inf = 1e9;
vector<int> adj[maxn];
int a[maxn];
int cnt[maxn][maxlog][2];
ll ans = 0;
void dfs(int v , int p){
    for(int i = 0 ; i < adj[v].size() ; i ++ ){
        int u = adj[v][i];
        if(u == p)
            continue;
        dfs(u , v);
        for(int i = 0 ; i < maxlog ; i ++ ){
            ans += (ll) cnt[u][i][0] * (ll) cnt[v][i][1] * (ll)(1 << i);
            ans += (ll) cnt[u][i][1] * (ll) cnt[v][i][0] * (ll)(1 << i);
            if(a[v] & (1 << i)){
                ans += (ll) cnt[u][i][0] * (ll) (1 << i);
                cnt[v][i][1] += cnt[u][i][0];
                cnt[v][i][0] += cnt[u][i][1];
            }
            else{
                ans += (ll) cnt[u][i][1] * (ll) (1 << i);
                cnt[v][i][1] += cnt[u][i][1];
                cnt[v][i][0] += cnt[u][i][0];
            }
        }
    }
    for(int i = 0 ; i < maxlog ; i ++ ){
        if(a[v] & (1 << i))
            cnt[v][i][1] ++ ;
        else
            cnt[v][i][0] ++ ;
    }
}
int main()
{
    int n ;
    cin >> n;
    ll sum = 0;
    for(int i = 1 ; i <= n ; i ++ ){
        cin >> a[i];
        sum += (ll) a[i];
    }
    for(int i = 0 ; i < n - 1 ; i ++ ){
        int u , v;
        cin >> u >> v;
        adj[u].pb(v) , adj[v].pb(u);
    }
    dfs(1 , 0);
    cout << ans + sum;
    return 0;
}
