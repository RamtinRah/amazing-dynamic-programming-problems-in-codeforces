///link to problem : https://codeforces.com/problemset/problem/413/D

#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back
using namespace std;
typedef long long ll;
typedef pair<int,int> pii;
typedef pair<ll,ll> pll;
typedef long double ld;
typedef map<ll,ll>::iterator mapit;
typedef set<pii>::iterator setit;
const int maxn = 3000;
const int inf=1e9;
const int mod=1e9+7;
int dp[maxn][maxn][2];
int a[maxn];
void sum(int &a , int &b)
{
    a = (a + b) % mod;
}
int main()
{
    ios_base::sync_with_stdio(0);cin.tie(0);cout.precision(20);
    int n,k; cin >> n >> k;
    k -- ;
    for(int i=0;i<n;i++)
    {
        cin >> a[i];
        a[i] /= 2;
    }
    if(n == 1 && a[0] == 1 && k == 1)
    {
        cout << 0 ;
        return 0;
    }
    if(k <= 1)
    {
        int ans = 0;
        for(int i=0;i<n;i++)
            if(a[i] == 0)
                ans = (ans * 2) % mod;
        cout << ans ;
        return 0;
    }
    if(a[0] == 1 || a[0] == 0)
        dp[0][1][0] = 1;
    if(a[0] == 2 || a[0] == 0)
        dp[0][2][0] = 1;
    for(int i=0;i<n-1;i++)
    {
        for(int mask = 0 ; mask < (1<<k) ; mask ++)
        {
            //cout << i << " " << mask << " " << dp[i][mask][0] << " " << dp[i][mask][1] << endl;
            if(a[i+1] == 1 || a[i+1] == 0)
            {
                sum(dp[i+1][(mask+1) % (1<<k)][1] , dp[i][mask][1]);
                if(mask + 1 == (1<<k))
                    sum(dp[i+1][0][1] , dp[i][mask][0]);
                else
                    sum(dp[i+1][mask+1][0] , dp[i][mask][0]);
            }
            if(a[i+1] == 2 || a[i+1] == 0)
            {
                if(mask % 2 == 1)
                {
                    sum(dp[i+1][2][1] , dp[i][mask][1]);
                    sum(dp[i+1][2][0] , dp[i][mask][0]);
                }
                else
                {
                    sum(dp[i+1][(mask+2) % (1<<k)][1] , dp[i][mask][1]);
                    if(mask + 2 == (1<<k))
                        sum(dp[i+1][0][1] , dp[i][mask][0]);
                    else
                        sum(dp[i+1][mask+2][0] , dp[i][mask][0]);
                }
            }
        }
    }
    int ans = 0;
    for(int mask = 0 ; mask < (1<<k) ; mask++)
        sum(ans , dp[n-1][mask][1]);
    cout << ans ;
    return 0;
}
