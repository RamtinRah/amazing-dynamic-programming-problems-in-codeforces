

///link to problem : https://codeforces.com/problemset/problem/601/C



#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back
using namespace std;
typedef long long ll;
typedef pair<int,int> pii;
typedef long double ld;
typedef map<int,int>::iterator mapit;
typedef set<int>::iterator setit;
const int maxn = 100 + 4;
const int maxm = 1e3 + 4;
const int inf = 1e7;
const ll mod = 1e9+7;
const ll base = 307;
ld dp[maxn][maxn * maxm];
ld pref[maxn][maxn * maxm];
int a[maxn];
int main()
{
    ios_base::sync_with_stdio(0);cin.tie(0);cout.precision(20);
    int n , m;
    cin >> n >> m;
    int sum = 0;
    for(int i=1;i<=n;i++){
        cin >> a[i];
        sum += a[i];
    }
    if(m == 1){
        cout << 1 ;
        return 0;
    }
    dp[0][0] = 1;
    pref[0][0] = 1;
    for(int j=1;j<=n*m;j++)
        pref[0][j] = 1;
    for(int i=1;i<=n;i++){
        for(int j=1;j<=n*m;j++){
            dp[i][j] = pref[i - 1][j - 1];
            if(j > m)
                dp[i][j] -= pref[i - 1][j - m - 1];
            if(j >= a[i])
                dp[i][j] -= dp[i - 1][j - a[i]];
            dp[i][j] /= (ld) (m - 1);
            pref[i][j] = pref[i][j - 1] + dp[i][j];
            //cout << i << " " << j << " " << dp[i][j] << " " << pref[i][j] << endl;
        }
    }
    ld p = 0;
    for(int i=1;i<sum;i++){
        p += dp[n][i];
    }
    p = p * (ld) (m - 1) + (ld)1;
    cout << p;
    return 0;
}
