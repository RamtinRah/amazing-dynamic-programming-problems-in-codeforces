///link to problem : https://codeforces.com/problemset/problem/53/E

#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back

using namespace std;

typedef long long ll;
typedef pair<ll,ll> pii;
typedef long double ld;
typedef map<ll,ll>::iterator mapit;
typedef set<ll>::iterator setit;

const int maxn = 11;
const int maxlog = 20;
const ll inf = 1e18;
const int mod = 1e9 + 7;
const ll base = 307;
int edge[maxn][maxn];
int dp[1<<maxn][1<<maxn];
int main()
{
    ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
    int n , m , k;
    cin >> n >> m >> k;
    while(m -- ){
        int u , v;
        cin >> u >> v;
        u -- , v -- ;
        edge[u][v] = true , edge[v][u] = true;
    }
    ll ans = 0;
    for(int mask1 = 0 ; mask1 < (1<<n) ; mask1 ++ ){
        for(int mask2 = 0 ; mask2 < (1<<n) ; mask2 ++){
            bool tof = false;
            for(int i=0;i<n;i++)
                if((mask2 & (1<<i)) && mask1 & (1<<i))
                    tof = true;
            if(tof)
                continue ;
            int cnt1 = __builtin_popcount(mask1) , cnt2 = __builtin_popcount(mask2);
            if(cnt2 < 2)
                continue ;
            int v = __builtin_ctz(mask2);
            if(cnt1 + cnt2 == 2){
                int u = __builtin_ctz(mask2 ^ (1 << v));
                if(edge[u][v])
                    dp[mask1][mask2] = 1;
                continue ;
            }
            for(int u=0;u<n;u++){
                if(edge[u][v] && mask1 & (1 << u))
                    dp[mask1][mask2] += dp[mask1][mask2 ^ (1 << v)] + dp[mask1 ^ (1 << u)][mask2 ^ (1 << v) ^ (1 << u)];
            }
            if(cnt1 + cnt2 == n && cnt2 == k)
                ans += dp[mask1][mask2];
        }
    }
    cout << ans;
    return 0;
}
