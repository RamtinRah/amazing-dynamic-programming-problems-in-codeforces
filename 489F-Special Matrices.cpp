

///link to problem : https://codeforces.com/problemset/problem/489/F



#include <bits/stdc++.h>
using namespace std;
#define pb push_back
typedef long long ll;
const int maxn=1500;
ll dp[maxn][maxn],mod;
int n,m,a[maxn];
int main()
{
    cin>>n>>m>>mod;
    for(int i=0;i<m;i++)
    {
        string s; cin>>s;
        for(int j=0;j<n;j++)
        {
            if(s[j]=='1')
                a[j]++;
        }
    }
    int one=0,two=0;
    for(int i=0;i<n;i++)
        if(a[i]==1)
            one++;
        else if(a[i]==0)
            two++;
    dp[0][0]=1;
    for(ll i=0;i<maxn;i++)
    {
        for(ll j=0;j<maxn;j++)
        {
            if(i>=2)
            {
                dp[i][j]=dp[i-2][j+2]*i*(i-1)/2;
                dp[i][j]=dp[i][j]%mod;
            }
            if(j>=2)
            {
                dp[i][j]+=dp[i][j-2]*j*(j-1)/2;
                dp[i][j]=dp[i][j]%mod;
            }
            if(i && j)
            {
                dp[i][j]+=dp[i-1][j]*i*j;
                dp[i][j]=dp[i][j]%mod;
            }
        }
    }
    cout<<dp[two][one];
    return 0;
}
