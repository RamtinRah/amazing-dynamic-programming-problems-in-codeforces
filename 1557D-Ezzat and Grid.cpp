

///link to problem : https://codeforces.com/problemset/problem/1557/D





#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back

using namespace std;

typedef long long ll;
typedef pair<int , int> pii;
typedef long double ld;
typedef map<pii,set<pii> >::iterator mapit;
typedef set<int>::iterator setit;


const int maxn = 6e5 + 43;
const int maxm = 3003;
const ll maxii = 1e11 ;
const int maxlog = 20;
const ll mod = 1e9 + 7;
const int sq = 350;
const int inf = 1e9 + 43 ;
const ld PI  =3.141592653589793238463;
const ld eps = 1e-12;
//fe
vector<pii> a[maxn / 2];
set<int> st;
map<int,int> ma;
int dp[maxn / 2] , prevv[maxn / 2];
int seg[4 * maxn] , lazy[4 * maxn];
bool prnt[maxn / 2];
int get(int ind , int l , int r , int x , int y){

    if(x <= l && r <= y)
        return seg[ind];
    if(r <= x || l >= y)
        return 0;
    if(lazy[ind]){
        lazy[2 * ind] = lazy[ind];
        lazy[2 * ind + 1] = lazy[ind];
        seg[2 * ind] = seg[ind];
        seg[2 * ind + 1] = seg[ind];
        lazy[ind] = 0;
    }
    int mid = (l + r) / 2;
    int res1 = get(2 * ind , l , mid , x , y);
    int res2 = get(2 * ind + 1 , mid , r , x , y);
    if(dp[res1] > dp[res2])
        return res1;
    else
        return res2;

}
void upd(int ind , int l , int r , int x , int y , int val){
    if(x <= l && r <= y){
        seg[ind] = val;
        lazy[ind] = val;
        return ;
    }
    if(r <= x || l >= y)
        return ;
    if(lazy[ind]){
        lazy[2 * ind] = lazy[ind];
        lazy[2 * ind + 1] = lazy[ind];
        seg[2 * ind] = seg[ind];
        seg[2 * ind + 1] = seg[ind];
        lazy[ind] = 0;
    }
    int mid = (l + r) / 2;
    upd(2 * ind , l , mid , x , y , val);
    upd(2 * ind + 1 , mid , r , x , y , val);
    if(dp[seg[2 * ind]] > dp[seg[2 * ind + 1]])
        seg[ind] = seg[2 * ind];
    else
        seg[ind] = seg[2 * ind + 1];

    return ;
}
int main(){
    ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
    int n , m;
    cin >> n >> m;

    for(int i = 0 ; i < m ; i ++ ){
        int x , l , r;
        cin >> x >> l >> r ;
        st.insert(l);
        st.insert(r);
        a[x].pb(mp(l , r));
    }
    int cur = 0;
    for(set<int> :: iterator it = st.begin() ; it != st.end() ; it ++ ){
        int num = *it;
        ma[num] = cur;
        cur ++ ;
    }

    int lst = 0 ;
    for(int i = 1 ; i <= n ; i ++ ){
        prnt[i] = true;
        if((int)a[i].size() == 0)
            dp[i] = 1;
        for(int j = 0 ; j < a[i].size() ; j ++ ){
            int l = ma[a[i][j].first];
            int r = ma[a[i][j].second];


            int maxi = get(1 , 0 , cur , l , r + 1);
      //      cout << "? " << maxi << endl;
            if(dp[i] < dp[maxi] + 1){
                dp[i] = dp[maxi] + 1;
                prevv[i] = maxi;
            }

        }

        if(dp[i] > dp[lst])
            lst = i;

        for(int j = 0 ; j < a[i].size() ; j ++ ){
            int l = ma[a[i][j].first];
            int r = ma[a[i][j].second];
            upd(1 , 0 , cur , l , r + 1 , i);

        }
     //   cout << "*   " << i << " " << dp[i] << "    " << prev[i] << endl;


    }

    cout << n - dp[lst] << endl;
    while(lst != 0){

        prnt[lst] = false;
        lst = prevv[lst];
    }
    for(int i = 0 ; i <= n ; i ++)
        if(prnt[i])
            cout << i << " ";


	return 0;
}
