///link to problem : https://codeforces.com/problemset/problem/8/C


#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back
using namespace std;
typedef long long ll;
typedef pair<int,int> pii;
typedef pair<ll,ll> pll;
typedef long double ld;
typedef map<ll,ll>::iterator mapit;
typedef set<pii>::iterator setit;
const int maxn = 25;
const int inf=1e9;
const ll mod=1e9+7;
int dp[1<<maxn] , way[1<<maxn];
int x[maxn] , y[maxn];
int main()
{
    ios_base::sync_with_stdio(0);cin.tie(0);cout.precision(20);
    int xs,ys; cin >> xs >> ys;
    int n; cin >> n;
    for(int i=0;i<n;i++)
        cin >> x[i] >> y[i];
    for(int mask = 1 ; mask < (1<<n) ; mask++)
    {
        int lb = __builtin_ctz(mask);
        way[mask] = lb;
        dp[mask] = dp[mask ^ (1<<lb)] +  2 * ((xs - x[lb]) * (xs - x[lb]) + (ys - y[lb]) * (ys - y[lb]));
        for(int i=0;i<n;i++)
        {
            if((mask & (1<<i)) && i!=lb)
            {
                int cur = dp[mask ^ (1<<lb) ^ (1<<i)] + (xs - x[lb]) * (xs - x[lb]) + (ys - y[lb]) * (ys - y[lb]) + (x[lb] - x[i]) * (x[lb] - x[i]) + (y[lb] - y[i]) * (y[lb] - y[i]) + (xs - x[i]) * (xs - x[i]) + (ys - y[i]) * (ys - y[i]);
                if(dp[mask] > cur)
                {
                    dp[mask] = cur;
                    way[mask] = i;
                }
            }
        }
    }
    int mask = (1<<n) - 1;
    cout << dp[mask] << endl;
    cout << 0 << " ";
    while(mask)
    {
        int lb = __builtin_ctz(mask);
        if(way[mask] == lb)
        {
            cout << lb + 1 << " " << 0 << " ";
            mask ^= (1<<lb);
        }
        else
        {
            cout << lb + 1 << " " << way[mask]+1 << " " << 0 << " ";
            mask = mask ^ (1<<lb) ^ (1<<way[mask]);
        }
    }
    return 0;
}
