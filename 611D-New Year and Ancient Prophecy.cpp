

///link to problem : https://codeforces.com/problemset/problem/611/D




#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back

using namespace std;

typedef long long ll;
typedef pair<int,int> pii;
typedef long double ld;
typedef map<ll,ll>::iterator mapit;
typedef set<int>::iterator setit;

const int maxn = 5000 + 5;
const int mod = 1e9 + 7;
const ll maxlog = 70;
const int sq = 350;
int z[maxn][maxn];
int dp[maxn][maxn] , sum_dp[maxn][maxn];
string s;
int main()
{
    ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);cout.precision(20);
    int n;
    cin >> n >> s;
    s.insert(s.begin() , '!');
    for(int i=n;i>0;i--){
        for(int j=i+1;j<=n;j++){
            if(s[i] != s[j])
                z[i][j] = 0;
            else if(j == n)
                z[i][j] = 1;
            else
                z[i][j] = 1 + z[i + 1][j + 1];
        }
    }
    dp[0][0] = 1;
    sum_dp[0][0] = 1;
    //cout << z[5][3] << endl;
    for(int i=1;i<=n;i++){
        for(int j=1;j<=i;j++){
                int cur = i - j + 1;
                if(s[cur] == '0')
                    continue;
                int pre = i - 2 * j + 1;
                if(pre <= 0)
                    dp[i][j] = sum_dp[i - j][i - j];
                else{
                    int k1 = z[pre][cur] + pre;
                    int k2 = z[pre][cur] + cur;
                    if(k2 > i){
                        dp[i][j] = sum_dp[i - j][j - 1];
                    }
                    else if(s[k1] > s[k2])
                        dp[i][j] = sum_dp[i - j][j - 1];
                    else
                        dp[i][j] = sum_dp[i - j][j];
                }
                //cout << i << " " << j << " " << dp[i][j] << endl;
        }
        for(int j=1;j<=i;j++)
            sum_dp[i][j] = (sum_dp[i][j - 1] + dp[i][j]) % mod;
    }
    cout << sum_dp[n][n];
    return 0;
}
