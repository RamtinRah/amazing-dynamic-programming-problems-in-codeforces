
///link to problem : https://codeforces.com/problemset/problem/1523/D





#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back

using namespace std;

typedef unsigned long long ll;
typedef pair<int , int> pii;
typedef long double ld;
typedef map<pii,set<pii> >::iterator mapit;
typedef multiset<ll>::iterator setit;


const int maxn = 2e5 + 43;
const int maxm = 3003;
const ll maxii = 1e11 ;
const int maxlog = 61;
const ll mod = 1e9 + 7;
const int sq = 350;
const int inf = 1e9 + 43 ;
const ld PI  =3.141592653589793238463;
const ld eps = 1e-12;
ll a[maxn];
char s[maxn];
char ans[maxn];
int cnt[maxn];
int cnt2[maxn];
int main(){
    ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);

    int n , m , p ;
    scanf("%d%d%d" , &n , &m , &p);
    srand(2114992323);
   // cin >> n >> m >> p;
    for(int i = 0 ; i < n ; i ++ ){
        a[i] = 0;
        scanf("%s",s);
       // cin >> s;
        //reverse(s.begin() , s.end());
        for(int j = 0 ; j < m ; j ++ ){
            if(s[j] == '1')
                a[i] ^= (1LL << j);
        }
    }
    ll best = 0 ;
    string bst ;
    for(int i = 0 ; i < m ; i ++ )
        bst += '0';

    for(int it = 0 ; it < 50 ; it ++ ){
        //srand(time(NULL));
        int rnd=1ll*rand()*rand()%n;
        vector<int> positions;
       // cout << "*    " << endl << endl << rnd << endl << endl << endl;

        for(int j = 0 ; j < m ; j ++ ){
            if(a[rnd] & (1LL << j)){
                positions.pb(j);

            }
        }
        int le = positions.size();
        for(int mask = 0 ; mask < (1 << le) ; mask ++ ){
            cnt[mask] = 0;
            cnt2[mask] = 0;
        }

        for(int i = 0 ; i < n ; i ++ ){
            ll mask = 0;
            for(int j = 0 ; j < le ; j ++ ){
                mask *= (ll)2;
                int pos = positions[j];
                if(a[i] & (1LL << pos))
                    mask ++ ;
            }
            cnt[mask] ++ ;
    //        cout << mask << endl;
        }

        for(ll mask = 0 ; mask < (1LL << le) ; mask ++ ){
            for(ll sub = mask ; sub ; sub = (sub - 1) & mask){
                cnt2[sub] += cnt[mask];
            }
        }
        reverse(positions.begin() , positions.end());
        ll best_mask = 0;
        for(ll mask = 0 ; mask < (1LL << le) ; mask ++ ){
          //  cout << mask << " " << cnt2[mask] << endl;
            if(cnt2[mask] >= (n + 1) / 2){
                if(__builtin_popcount(mask) > __builtin_popcount(best_mask)){
                    best_mask = mask;

                }
            }
        }
        if(__builtin_popcount(best_mask) > __builtin_popcount(best)){

                    best = best_mask;
                    string cur ;
                    for(int j = 0 ; j < m ; j ++)
                        cur += '0';
                    for(int j = 0 ; j < le ; j ++ ){
                        if(best_mask & (1LL << j)){
                            cur[positions[j]] = '1';
                        }
                    }
                    bst = cur;
                }


    }
    for(int i = 0 ; i < bst.size() ; i ++ )
        ans[i] = bst[i];
    printf("%s",ans);
   // cout << bst << endl;
	return 0;
}
