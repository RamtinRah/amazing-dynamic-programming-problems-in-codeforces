
///link to problem : https://codeforces.com/problemset/problem/1156/D


#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back

using namespace std;

typedef long long ll;
typedef pair<int , int> pii;
typedef long double ld;
typedef map<pii,set<pii> >::iterator mapit;
typedef multiset<ll>::iterator setit;

const int maxn = 2e5 + 43;
const int maxm = 2e3 + 43;

const int maxlog = 22;
const ll mod = 1e9 + 7;
const int sq = 340;
const int inf = 1e9 ;
const ld PI  =3.141592653589793238463;
vector<pii> adj[maxn];
ll cnt0[maxn] , cnt1[maxn] , cnt01[maxn] , cnt10[maxn];
ll ans = 0;
void dfs(int v , int p){
    for(int i = 0 ; i < adj[v].size() ; i ++ ){
        int u = adj[v][i].first;
        int x = adj[v][i].second;
        if(u == p)
            continue;
        dfs(u , v);
        if(x == 0){
            ll cur = cnt0[u] + 1;
            ll cur2 = cnt1[u] + cnt01[u]; /// to add to cnt01
            ans += cur * cnt0[v] * 2 ; ///0 0
            ans += cur * cnt1[v] ; ///1 0
            ans += cnt0[v] * cur2 ; ///0 01
            ans += cnt01[v] * cur ; ///10 0
            ans += cur * 2 + cur2; /// to v
            cnt0[v] += cur;
            cnt01[v] += cur2;
        }
        else if(x == 1){
            ll cur = cnt1[u] + 1;
            ll cur2 = cnt0[u] + cnt10[u]; /// to add to cnt10
            ans += cur * cnt1[v] * 2 ; ///1 1
            ans += cur * cnt0[v] ; ///0 1
            ans += cnt1[v] * cur2 ; ///1 10
            ans += cnt10[v] * cur ; ///01 1
            ans += cur * 2 + cur2; /// to v
            cnt1[v] += cur;
            cnt10[v] += cur2;
        }
    }

    //cout << v << " " << cnt0[v] << " " << cnt1[v] << " " << ans[v] << endl;
}
int main(){
    ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);cout.precision(15);
    int n;
    cin >> n ;
    for(int i = 0 ; i < n - 1 ; i ++ ){
        int u , v , x;
        cin >> u >> v >> x;
        adj[u].pb(mp(v , x));
        adj[v].pb(mp(u , x));
    }
    dfs(1 , 0);
    cout << ans;
    return 0;
}
