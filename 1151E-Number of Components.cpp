
///link to problem : https://codeforces.com/problemset/problem/1151/E


#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back

using namespace std;

typedef long long ll;
typedef pair<int , int> pii;
typedef long double ld;
typedef map<pii,set<pii> >::iterator mapit;
typedef multiset<ll>::iterator setit;

const int maxn = 1e5 + 43;
const int maxm = 2e3 + 43;

const int maxlog = 22;
const ll mod = 1e9 + 7;
const int sq = 340;
const int inf = 1e9 ;
const ld PI  =3.141592653589793238463;
ll a[maxn];
int main(){
    ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);cout.precision(15);
    ll n ;
    cin >> n;
    ll ans = 0;
    for(ll i = 1 ; i <= n ; i ++ ){
        cin >> a[i];
        if(a[i] > a[i - 1]){
            ans += (a[i] - a[i - 1]) * (n - a[i] + 1);
        }
        else
            ans += a[i] * (a[i - 1] - a[i]);
    }
    cout << ans;
    return 0;
}
