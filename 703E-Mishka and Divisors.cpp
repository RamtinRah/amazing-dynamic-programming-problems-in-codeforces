///link to problem : https://codeforces.com/problemset/problem/703/E


#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back

using namespace std;

typedef long long ll;
typedef pair<ll,ll> pii;
typedef long double ld;
typedef map<string , vector<int> >::iterator mapit;
typedef set< pair<int , int> >::iterator setit;

const int maxn = 1e3 + 34;
const int maxlog = 20;
const int mod = 1e9 + 7;
const int sq = 720;
const int inf = 1e9;
int dp[maxn][(int)1e4 + 5] , pre[maxn][(int)1e4 + 5];
ll sum[maxn][(int)1e4 + 5];
vector<ll> divisors;
int tof[(int)1e6 + 76];
ll a[maxn];
ll k , k2;
vector<ll> primes;
int gettof(ll a){
    if(a <= sqrt(k2))
        return tof[a];
    else
        return tof[k2 / a] + 1;
}
vector<int> v[1000 * maxn];
vector<int> get(ll num){
    vector<int> ans;
    for(int j = 0 ; j < primes.size() ; j ++ ){
        int cnt = 0;
        while(num % primes[j] == 0){
            cnt ++ ;
            num /= primes[j];
        }
        ans.pb(cnt);
    }
    return ans;
}
int main()
{
    ios_base::sync_with_stdio(false) , cin.tie(0) , cout.tie(0); cout.precision(20);
    int n ;
    cin >> n >> k;
    k2 = k;
    ///tarahe koskesh :|
    ll cur = 0;
    for(ll i = 1 ; i * i <= k ; i ++ ){
        if(k % i == 0){
            divisors.pb(i);
            tof[i] = cur;
            cur +=2;
            divisors.pb(k / i);
            if(k / i == i)
                divisors.pop_back();
        }
    }

    //
    ll k3 = k;
    cur = 0;
    for(ll i = 2 ; i < 1e6 + 6 ; i ++ ){
        if(k3 % i == 0){
            primes.pb(i);
            cur ++ ;
        }
        while(k3 % i == 0)
            k3 /= i;
    }
    if(k3 != 1)
        primes.pb(k3);
    //

    for(int i = 0 ; i < divisors.size() ; i ++ ){
        ll cur = divisors[i];
        v[i] = get(cur);
    }









    dp[0][gettof(1)] = 0;
    for(int i = 1 ; i < divisors.size() ; i ++ )
        dp[0][gettof(divisors[i])] = inf;
    for(int i = 1 ; i <= n ; i ++ ){
        cin >> a[i];
        vector<int> vv = get(a[i]);
       // dp[mp(i , 1)] = 1;
        for(int j = 0 ; j < divisors.size() ; j ++ ){
            ll d = divisors[j];


            ll g = 1;
            for(int l = 0 ; l < primes.size() ; l ++ ){
                int m = min(vv[l] , v[j][l]);
                while(m -- )
                    g *= primes[l];
            }




            ll d2 = gettof(d) , dg = gettof(d / g);
            if(dp[i - 1][d2] < 1 + dp[i - 1][dg]){
                pre[i][d2] = 1;
                sum[i][d2] = sum[i - 1][d2];
            }
            else if(dp[i - 1][d2] > 1 + dp[i - 1][dg]){
                pre[i][d2] = 2;
                sum[i][d2] = a[i] + sum[i - 1][dg];
            }
            else if(sum[i - 1][d2] < a[i] + sum[i - 1][dg]){
                pre[i][d2] = 1;
                sum[i][d2] = sum[i - 1][d2];
            }
            else{
                pre[i][d2] = 2;
                sum[i][d2] = a[i] + sum[i - 1][dg];
            }
            dp[i][d2] = min(dp[i - 1][d2] , 1 + dp[i - 1][dg]);
        }
    }
    if(k == 1){
        cout << 1 << endl;
        int minpl = 1;
        for(int i = 2 ; i <= n ; i ++ )
            if(a[i] < a[minpl])
                minpl = i;
        cout << minpl;
        return 0;
    }
    if(dp[n][gettof(k)] == inf){
        cout << -1;
        return 0;
    }
    cout << dp[n][gettof(k)] << endl;
    for(int i = n ; i > 0 ; i -- ){
        if(pre[i][gettof(k)] == 2){
            cout << i << " ";
            ll g = __gcd(k , a[i]);
            k /= g;
        }
    }
    return 0;
}
