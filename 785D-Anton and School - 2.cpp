
///link to problem : https://codeforces.com/contest/785/problem/D

#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back

using namespace std;

typedef long long ll;
typedef pair<int,int> pii;
typedef long double ld;
typedef map<pii,set<pii> >::iterator mapit;
typedef multiset<ll>::iterator setit;

const int maxn = 4e5 + 43;
const int maxlog = 25;
const ll mod = 1e9 + 7;
const int sq = 720;
const int inf = 1e9;
ll dp1[maxn] , dp2[maxn];
ll fact[maxn];
ll powmod(ll a , ll b){
    if(b == 0)
        return 1;
    ll cur = powmod(a , b / 2);
    cur = (cur * cur) % mod;
    if(b & 1)
        cur = (cur * a) % mod;
    return cur;
}
ll comb(ll a , ll b){
    ll cur = fact[a] * fact[b - a];
    cur %= mod;
    return (fact[b] * powmod(cur , mod - 2)) % mod;
}
int main()
{
    ios_base::sync_with_stdio(0);cin.tie(0);cout.precision(20);cout.tie(0);
    string s;
    cin >> s;
    s.insert(s.begin() , '!');
    fact[0] = 1;
    for(ll i = 1 ; i < s.size() ; i ++ ){
        dp1[i] += dp1[i - 1];
        dp2[i] += dp2[i - 1];
        if(s[i] == '(')
            dp1[i] ++ ;
        else
            dp2[i] ++ ;
    }
    for(ll i = 1 ; i < 2 * s.size() ; i ++ )
        fact[i] = (fact[i - 1] * i) % mod;

    ll ans = 0;
    for(int i = 1 ; i < s.size() ; i ++ ){
        if(s[i] == '(')
           continue;
        ll m = dp1[i - 1];
        ll n = dp2[(int) s.size() - 1] - dp2[i];
        //cout << i << " " <<
        if(m >= 1)
            ans += comb(m - 1 , m + n);
        ans %= mod;
        //cout << ans << " ";
    }
    cout << ans;
    return 0;
}
