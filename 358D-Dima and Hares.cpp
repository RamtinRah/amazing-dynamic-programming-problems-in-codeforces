///link to problem : https://codeforces.com/contest/358/problem/D


#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back
using namespace std;
typedef long long ll;
typedef pair<int,int> pii;
typedef long double ld;
typedef map<int,int>::iterator mapit;
typedef set<int>::iterator setit;
const int maxn=3e3+4;
const ll INF=1e18+4;
const int MOD = 1e9+7;
const ld eps=1e-9;
const ld pi=3.1415926535897932384626433832795;
int dp[maxn][2];
int a[maxn],b[maxn],c[maxn];
int main()
{
    ios_base::sync_with_stdio(0);cin.tie(0);cout.precision(20);
    int n; cin>>n;
    for(int i=0;i<n;i++)
        cin>>a[i];
    for(int i=0;i<n;i++)
        cin>>b[i];
    for(int i=0;i<n;i++)
        cin>>c[i];
    dp[0][0] = a[0];
    dp[0][1] = -MOD;
    for(int i=1;i<n;i++)
    {
        dp[i][0] = a[i] + max(dp[i-1][0]-a[i-1]+b[i-1],dp[i-1][1]-b[i-1]+c[i-1]);
        dp[i][1] = b[i] + max(dp[i-1][0],dp[i-1][1]);
    }
    cout<<max(dp[n-1][0],dp[n-1][1]);
    return 0;
}
