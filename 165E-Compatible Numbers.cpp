///link to problem : https://codeforces.com/problemset/problem/165/E


#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back

using namespace std;

typedef long long ll;
typedef pair<int,int> pii;
typedef long double ld;
typedef map<pii,set<pii> >::iterator mapit;
typedef multiset<ll>::iterator setit;

const int maxn = 1e6 + 5;
const int maxlog = 22;
const ll mod = 1e9 + 7;
const int sq = 340;
const ll inf = 2e18 ;
const ld pi = 3.14159265;

int dp[1 << maxlog];
int a[maxn];
int main()
{
    ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);cout.precision(15);
    int n ; cin >> n;
    int maxi = (1 << maxlog) - 1;
    for(int mask = 0 ; mask <= maxi ; mask ++ )
        dp[mask] = -1;

    for(int i = 0 ; i < n ; i ++ ){
        cin >> a[i];
        dp[maxi ^ a[i]] = a[i];
    }
    for(int mask = maxi ; mask >= 0 ; mask -- )
        for(int i = 0 ; i < maxlog ; i ++ )
            if(!(mask & (1 << i)))
                if(dp[mask ^ (1 << i)] != -1)
                    dp[mask] = dp[mask ^ (1 << i)];
    for(int i = 0 ; i < n ; i ++ )
        cout << dp[a[i]] << " ";
    return 0;
}
