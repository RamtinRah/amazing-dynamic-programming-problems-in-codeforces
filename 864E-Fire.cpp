
///link to problem : https://codeforces.com/problemset/problem/864/E

#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back

using namespace std;

typedef long long ll;
typedef pair<int,int> pii;
typedef long double ld;
typedef map<pii,set<pii> >::iterator mapit;
typedef multiset<ll>::iterator setit;

const int maxn = 104;
const int maxlog = 20;
const ll mod = 1e9 + 7;
const int sq = 340;
const ll inf = 4e18;
const ld pi = 3.14159265;
int dp[maxn][20 * maxn] , ty[maxn][20 * maxn];
struct S{
    int t , d , val , id ;
} a[maxn];
bool cmp(S a , S b){
    return a.d < b.d;
}
int main()
{
    ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
    int n ;
    cin >> n;
    for(int i = 1 ; i <= n ; i ++ ){
        cin >> a[i].t >> a[i].d >> a[i].val;
        a[i].id = i ;
    }
    sort(a + 1 , a + n + 1 , cmp);
    int ans = 0;
    for(int i = 1 ; i <= n ; i ++ ){
        for(int j = 0 ; j < 20 * maxn ; j ++ ){
            dp[i][j] = dp[i - 1][j];
            if(j >= a[i].t && j < a[i].d){
                if(dp[i - 1][j - a[i].t] + a[i].val > dp[i][j]){
                    ty[i][j] = 1;
                    dp[i][j] = dp[i - 1][j - a[i].t] + a[i].val ;
                }
            }
            ans = max(ans , dp[i][j]);
        }
    }
    vector<int> v;
    int cur = 0 ;
    for(int i = 0 ; i < 20 * maxn ; i ++ ){
        if(dp[n][i] == ans)
            cur = i;
    }
    for(int i = n ; i >= 1 ; i -- ){
        if(ty[i][cur] == 1){
            v.pb(a[i].id);
            cur = cur - a[i].t ;
        }
    }
    cout << ans << endl;
    cout << v.size() << endl;
    reverse(v.begin() , v.end());
    for(int i = 0 ; i < v.size() ; i ++ ){
        cout << v[i] << " ";
    }
    return 0;
}
