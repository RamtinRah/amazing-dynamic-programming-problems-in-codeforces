
///link to problem : https://codeforces.com/problemset/problem/1433/F


#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back

using namespace std;

typedef long long ll;
typedef pair<ll , ll> pii;
typedef long double ld;
typedef map<pii,set<pii> >::iterator mapit;
typedef multiset<ll>::iterator setit;

const int maxn = 72;
const int maxm = 2e3 + 30;
const ll maxii = 1e18 ;
const int maxlog = 22;
const ll mod = (ll)1e9 + 7;
const int sq = 340;
const int inf = 1e9 ;
const ld PI  =3.141592653589793238463;
int dp2[maxn][maxn][maxn][maxn];
int a[maxn][maxn];
int dp[maxn][maxn];
void pre(){
    for(int i = 0 ; i < maxn ; i ++ ){
        for(int j = 0 ; j < maxn ; j ++ ){
            for(int k = 0 ; k < maxn ; k ++ ){
                for(int k2 = 0 ; k2 < maxn ; k2 ++ ){
                    dp2[i][j][k][k2] = -1;
                }
            }
            dp[i][j] = -1;
        }
    }

}
int main(){
    ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);cout.precision(15);
    pre();
    int n , m , k;
    cin >> n >> m >> k;
    for(int i = 0 ; i < n ; i ++ ){
        for(int j = 0 ; j < m ; j ++ ){
            cin >> a[i][j];
        }
    }
    int t = m / 2;
    for(int row = 0 ; row < n ; row ++ ){
        for(int i = 0 ; i < m ; i ++ ){
            for(int cnt = 0 ; cnt <= t ; cnt ++ ){
                dp2[row][i][cnt][0] = 0;
            }
        }
        for(int cnt = 1 ; cnt <= t ; cnt ++ ){
            dp2[row][0][cnt][a[row][0] % k] = a[row][0];
        }
        for(int i = 1 ; i < m ; i ++ ){
            for(int cnt = 1 ; cnt <= t ; cnt ++ ){
                for(int r = 0 ; r < k ; r ++ ){
                    if(dp2[row][i - 1][cnt - 1][(r - (a[row][i] % k) + k) % k] != -1)
                        dp2[row][i][cnt][r] = max(dp2[row][i - 1][cnt][r] , dp2[row][i - 1][cnt - 1][(r - (a[row][i] % k) + k) % k] + a[row][i]);
                    else
                        dp2[row][i][cnt][r] = dp2[row][i - 1][cnt][r];
                }
            }
        }
    }
    for(int i = 0 ; i < n ; i ++ ){
        dp[i][0] = 0;
    }
    for(int r = 0 ; r < k ; r ++ ){
        dp[0][r] = dp2[0][m - 1][t][r];
    }
    for(int row = 1 ; row < n ; row ++ ){
        for(int r1 = 0 ; r1 < k ; r1 ++ ){
            for(int r2 = 0 ; r2 < k ; r2 ++ ){
                if(dp2[row][m - 1][t][r2] != -1 && dp[row - 1][((r1 - r2) % k + k) % k] != -1)
                    dp[row][r1] = max(dp[row][r1] , dp2[row][m - 1][t][r2] + dp[row - 1][((r1 - r2) % k + k) % k]);
            }
        }
    }
    cout << dp[n - 1][0]  << endl;
	return 0;

}
