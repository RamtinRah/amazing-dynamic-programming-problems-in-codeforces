///link to problem : https://codeforces.com/problemset/problem/11/D


#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back
using namespace std;
typedef long long ll;
typedef pair<int,int> pii;
typedef pair<ll,ll> pll;
typedef long double ld;
typedef map<ll,ll>::iterator mapit;
typedef set<int>::iterator setit;
const int maxn = 19;
const ll inf=1e18;
const int mod=1e9+7;
ll dp[1<<maxn][maxn] ;
ll ans;
bool edge[maxn][maxn] , in_mask[maxn];
int main()
{
    ios_base::sync_with_stdio(0);cin.tie(0);cout.precision(20);cout.tie(0);
    int n,m; cin >> n >> m;
    for(int i=0;i<m;i++)
    {
        int u,v; cin >> u >> v;
        u -- , v -- ;
        edge[u][v] = true , edge[v][u] = true;
    }
    for(int i=0;i<n;i++)
        dp[1<<i][i] = 1;
    for(int mask=1;mask<(1<<n);mask++)
    {
        int lb = __builtin_ctz(mask);
        for(int i=lb+1;i<n;i++)
        {
            if(mask & (1<<i))
            {
                for(int j=0;j<n;j++)
                    if(mask & (1<<j) && edge[i][j])
                        dp[mask][i] += dp[mask ^ (1<<i)][j];
                if(edge[lb][i] && __builtin_popcount(mask) > 2)
                    ans += dp[mask][i];
            }
        }
    }
    cout << ans / 2;
    return 0;
}
