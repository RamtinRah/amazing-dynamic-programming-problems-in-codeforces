
///link to problem : https://codeforces.com/contest/1598/problem/E


#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back

using namespace std;

typedef long long ll;
typedef pair<int , int> pii;
typedef long double ld;
typedef map<pii,set<pii> >::iterator mapit;
typedef set<int>::iterator setit;


const int maxn = 1e3 + 43;
const int maxm = 1003;
const ll maxii = 1e11 ;
const int maxlog = 20;
const ll mod = 1e9 + 7;
const int sq = 350;
const int inf = 1e6 ;
const ld PI  =3.141592653589793238463;
const ld eps = 1e-12;

ll dp[maxn][maxn] , dp1[maxn][maxn] , dp2[maxn][maxn];
int a[maxn][maxn];
ll ans = 0;
void fix(int x , int y){
    ans -= dp[x][y];
    if(a[x][y] == 1){
        dp[x][y] = dp2[x][y + 1] + dp1[x + 1][y] + 1;
        dp1[x][y] = dp2[x][y + 1] + 1;
        dp2[x][y] = dp1[x + 1][y] + 1;
    }
    else{
        dp[x][y] = 0;
        dp1[x][y] = 0;
        dp2[x][y] = 0;
    }
    ans +=  dp[x][y];
    return ;
}
int main(){
    ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
    int n , m , q;
    cin >> n >> m >> q;
    for(int i = n ; i >= 1 ; i -- ){
        for(int j = m ; j >= 1 ; j -- ){
            a[i][j] = 1;
            fix(i , j);
        }
    }
   // cout << ans << endl;
    while(q -- ){
        int x , y;
        cin >> x >> y;
        a[x][y] ^= 1;
        while(x >= 1 && y >= 1){
            fix(x , y);
            if(x != 1)
                fix(x - 1 , y);
            if(y != 1)
                fix(x , y - 1);
            x -- ;
            y -- ;
        }
        cout << ans << endl;

    }
    return 0;
}
