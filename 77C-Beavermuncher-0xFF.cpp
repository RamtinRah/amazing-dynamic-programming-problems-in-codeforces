
///link to problem : https://codeforces.com/problemset/problem/77/C


#include <bits/stdc++.h>
#define mp make_pair

#define pb push_back

using namespace std;

typedef long long ll;
typedef long double ld;
typedef pair<int,int> pii;

const int maxn = 2e5 + 2;
const ll inf = 1e18 + 43;
const int mod = 1e9+7;
const int maxlog = 20;
const ld pi = 3.1415926535897932384626433832795028841971693993751;
ll a[maxn];
vector<int> adj[maxn];
ll dp[maxn] , r[maxn];
void dfs(int v , int p){
    vector<ll> vec;
    ll sum_r = 0;
    for(int i = 0 ; i < adj[v].size() ; i ++ ){
        int u = adj[v][i];
        if(u == p)
            continue;
        a[u] -- ;
        dfs(u , v);
        vec.pb(1 + dp[u]);
        sum_r += r[u];
    }
    r[v] = a[v];
    ll cnt = min(a[v] , (ll) vec.size());
    r[v] -= cnt;
    sort(vec.begin() , vec.end());
    reverse(vec.begin() , vec.end());
    for(int i = 0 ; i < cnt ; i ++ )
        dp[v] += vec[i];
    dp[v] += cnt;
    cnt = min(r[v] , sum_r);
    r[v] -= cnt;
    dp[v] += 2 * cnt;
}
int main()
{
    ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);cout.precision(20);
    int n;
    cin >> n;
    for(int i = 1 ; i <= n ; i ++)
        cin >> a[i];
    for(int i = 0 ; i < n - 1 ; i ++ ){
        int u , v;
        cin >> u >> v;
        adj[u].pb(v);
        adj[v].pb(u);
    }
    int s;
    cin >> s;
    dfs(s , 0);
    cout << dp[s];
    return 0;
}
