
///link to problem : https://codeforces.com/problemset/problem/18/E





#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back
using namespace std;
typedef long long ll;
typedef pair<int,int> pii;
typedef long double ld;
typedef map<int,int>::iterator mapit;
typedef set<int,int>::iterator setit;
const int maxn=500+6;
const int inf=1e9;
int a[maxn][maxn];
int dp[maxn][30][30];
int ans=inf;
pii prt[maxn][30][30];
string s[maxn];
int main()
{
    ios_base::sync_with_stdio(0);cin.tie(0);cout.precision(20);
    int n,m; cin>>n>>m;
    for(int i=1;i<=n;i++)
    {
        string s; cin>>s;
        for(int j=0;j<m;j++)
            a[i][j] = s[j]-'a';
    }
    int ans_ind1=0,ans_ind2=0;
    for(int i=1;i<=n;i++)
    {
        for(int num1=0;num1<26;num1++)
        {
            for(int num2=0;num2<26;num2++)
            {
                for(int j=0;j<m;j++)
                {
                    if(j%2==0 && a[i][j]!=num1)
                        dp[i][num1][num2]++;
                    if(j%2==1 && a[i][j]!=num2)
                        dp[i][num1][num2]++;
                }
                int ind1=0,ind2=0;
                for(int num3=0;num3<26;num3++)
                {
                    for(int num4=0;num4<26;num4++)
                    {
                        if(num3!=num1 && num4!=num2)
                        {
                            if(dp[i-1][num3][num4]<=dp[i-1][ind1][ind2])
                            {
                                ind1=num3;
                                ind2=num4;
                            }
                        }
                    }
                }
                dp[i][num1][num2]+=dp[i-1][ind1][ind2];
                if(num1==num2)
                    dp[i][num1][num2]=inf;
                prt[i][num1][num2]=mp(ind1,ind2);
                if(i==n)
                {
                    if(dp[i][num1][num2]<=dp[i][ans_ind1][ans_ind2])
                    {
                        ans_ind1=num1;
                        ans_ind2=num2;
                    }
                }
            }
        }
    }
    cout<<dp[n][ans_ind1][ans_ind2]<<endl;
    for(int i=n;i>0;i--)
    {
        for(int j=0;j<m;j++)
        {
            if(j%2==0)
                s[i]+=(char)(ans_ind1+'a');
            else
                s[i]+=(char)(ans_ind2+'a');
        }
        int ind1=prt[i][ans_ind1][ans_ind2].first;
        int ind2=prt[i][ans_ind1][ans_ind2].second;
        ans_ind1=ind1;
        ans_ind2=ind2;
    }
    for(int i=1;i<=n;i++)
    {
        for(int j=0;j<m;j++)
            cout<<s[i][j];
        cout<<endl;
    }
    return 0;
}
