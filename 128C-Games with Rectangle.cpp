///link to problem : https://codeforces.com/problemset/problem/128/C

#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back

using namespace std;

typedef long long ll;
typedef pair<ll,ll> pii;
typedef long double ld;
typedef map<ll,ll>::iterator mapit;
typedef set<int>::iterator setit;

const int maxn = 2e3 + 5 ;
const int maxc = 61;
const int maxlog = 20;
const int mod = 1e9 + 7;
const int sq = 350 ;
const ld pi = 3.14159265358979323846264;
int comb[maxn][maxn];
int main()
{
    ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);cout.precision(20);
    int n , m , k;
    cin >> n >> m >> k;
    for(int i=0;i<maxn;i++){
        comb[i][0] = 1;
        for(int j=1;j<=i;j++)
            comb[i][j] = (comb[i - 1][j - 1] + comb[i - 1][j]) % mod;
    }
    ll ans = ((ll) comb[n - 1][2 * k] * (ll)comb[m - 1][2 * k]) % (ll) mod ;
    cout << ans;
    return 0;
}
