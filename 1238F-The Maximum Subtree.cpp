///link to problem : https://codeforces.com/problemset/problem/1238/F



#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back

using namespace std;

typedef long long ll;
typedef pair<int , int> pii;
typedef long double ld;
typedef map<pii,set<pii> >::iterator mapit;
typedef multiset<ll>::iterator setit;

const int maxn = 3e5 + 43;
const int maxm = 2e3 + 43;

const int maxlog = 22;
const ll mod = (ll)1e9 + 7;
const int sq = 340;
const int inf = 1e9 ;
const ld PI  =3.141592653589793238463;
int dp[maxn] , dp1[maxn];
vector<int> adj[maxn];
int ans = 0;
void dfs(int v , int p){
    dp1[v] = 1;
    vector<int> dps;
    for(int i = 0 ; i < adj[v].size() ; i ++ ){
        int u = adj[v][i];
        if(u == p) continue;
        dfs(u , v);
        dp1[v] = max(dp1[v] , dp1[u] + 1 + max(0 , (int)adj[u].size() - 2));
        dps.pb(dp1[u] +  max(0 , (int)adj[u].size() - 2));
    }
    sort(dps.begin() , dps.end());
    reverse(dps.begin() , dps.end());
    dp[v] = dp1[v] + (int)adj[v].size() - 1;
    if(dps.size() >= 2)
        dp[v] = max(dp[v] , dps[0] + dps[1] + 1 + (int)adj[v].size() - 2);
    //cout << v << " " << dp1[v] << " " << dp[v] << endl;
    ans = max(ans , dp[v]);
}
int main(){
    ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);cout.precision(15);
    int t;
    cin >> t;
    while(t -- ){
        int n;
        cin >> n;
        for(int i = 1 ; i <= n ; i ++ ){
            dp[i] = 0 ;
            dp1[i] = 0;
            adj[i].clear();
        }
        ans = 0;
        for(int i = 0 ; i < n - 1 ; i ++ ){
            int u , v;
            cin >> u >> v;
            adj[u].pb(v);
            adj[v].pb(u);
        }
        dfs(1 , 0);
        cout << ans << endl;
    }
    return 0;
}
