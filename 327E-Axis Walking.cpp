

///link to problem : https://codeforces.com/problemset/problem/327/E





#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back
using namespace std;
typedef long long ll;
typedef pair<int,int> pii;
typedef pair<ll,ll> pll;
typedef long double ld;
typedef map<ll,ll>::iterator mapit;
typedef set<int>::iterator setit;
const int maxn = 25;
const ll inf=1e18;
const int mod=1e9+7;
ll a[maxn] , b[maxn] ;
int dp[1<<maxn];
int k;
bool bad(ll sum)
{
    bool flag = false;
    for(int i=0 ; i<k ;i++)
        if(sum == b[i])
            flag = true;
    return flag;
}
int main()
{
    ios_base::sync_with_stdio(0);cin.tie(0);cout.precision(20);
    int n; cin >> n;
    for(int i=0;i<n;i++)
        cin >> a[i];
    cin >> k;
    for(int i=0;i<k;i++)
        cin >> b[i];
    dp[0] = 1;
    for(int mask = 1 ; mask < (1<<n) ; mask ++)
    {
        ll sum = 0;
        for(int i=0;i<n;i++)
        {
            if(mask & (1<<i))
            {
                dp[mask] += dp[mask ^ (1<<i)];
                dp[mask] %= mod;
                sum += a[i];
            }
        }
        if(bad(sum))
            dp[mask] = 0;
    }
    cout << dp[(1<<n) - 1];
    return 0;
}
