
///link to problem : https://codeforces.com/problemset/problem/1089/A






#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back

using namespace std;

typedef long long ll;
typedef pair<int , int> pii;
typedef long double ld;
typedef map<pii,set<pii> >::iterator mapit;
typedef set<int>::iterator setit;


const int maxn = 200 + 43;
const int maxm = 1003;
const ll maxii = 1e11 ;
const int maxlog = 20;
const ll mod = 1e9 + 7;
const int sq = 350;
const int inf = 1e9 ;
const ld PI  =3.141592653589793238463;
const ld eps = 1e-12;
bool dp[4][4][maxn][maxn];
pii mov[4][4][maxn][maxn];
bool ok(int s1 , int s2 , int p1 , int p2){
    if(s1 < 0 || s1 > 3 || s2 < 0 || s2 > 3 || p1 < 0 || p2 < 0)
        return false;
    if(dp[s1][s2][p1][p2] == false)
        return false;
    return true;
}
int main(){
    ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
    dp[0][0][0][0] = true;
    for(int s1 = 0 ; s1 <= 3 ; s1 ++ ){
        for(int s2 = 0 ; s2 <= 3 ; s2 ++ ){
            if(s1 + s2 == 0 || s1 + s2 == 6)
                continue;
            int lim = 25;
            if(s1 + s2 == 5)
                lim = 15;
            for(int p1 = 0 ; p1 < maxn ; p1 ++ ){
                for(int p2 = 0 ; p2 < maxn ; p2 ++ ){
                    ///now current match score
                    ///25   <25
                    if(s2 != 3){
                        for(int c2 = 0 ; c2 < lim - 1 ; c2 ++ ){
                            if(ok(s1 - 1 , s2 , p1 - lim , p2 - c2)){
                                dp[s1][s2][p1][p2] = true;
                                //cout << s1 << " " << s2 << " " << p1 << " " << p2 << endl;
                                mov[s1][s2][p1][p2] = mp(lim , c2);
                                break;
                            }
                        }
                    }
                    ///<25 25
                    if(s1 != 3){
                        for(int c1 = 0 ; c1 < lim - 1 ; c1 ++ ){
                            if(ok(s1 , s2 - 1 , p1 - c1 , p2 - lim)){
                                dp[s1][s2][p1][p2] = true;
                                mov[s1][s2][p1][p2] = mp(c1 , lim);
                                break;
                            }
                        }
                    }
                    ///ziad ziad - 2
                    if(s2 != 3){
                        for(int c1 = lim + 1 ; c1 <= p1 ; c1 ++ ){
                            if(ok(s1 - 1 , s2 , p1 - c1 , p2 - c1 + 2)){
                                dp[s1][s2][p1][p2] = true;
                                mov[s1][s2][p1][p2] = mp(c1 , c1 - 2);
                                break;
                            }
                        }
                    }

                    ///ziad - 2    ziad
                    if(s1 != 3){
                        for(int c2 = lim + 1 ; c2 <= p2 ; c2 ++ ){
                            if(ok(s1 , s2 - 1 , p1 - c2 + 2 , p2 - c2)){
                                dp[s1][s2][p1][p2] = true;
                                mov[s1][s2][p1][p2] = mp(c2 - 2 , c2);
                                break;
                            }
                        }
                    }
                }
            }
        }
    }

    int q;
    cin >> q;
    while(q -- ){
        int p1 , p2;
        cin >> p1 >> p2;
        bool done = false;
        for(int diff = 3 ; diff >= -3 ; diff -- ){
            if(done) break;
            for(int s1 = 3 ; s1 >= 0 ; s1 -- ){
                if(done) break;
                int s2 = s1 - diff;
                if(s2 < 0 || s2 > 3)
                    continue;
                if(max(s1 , s2) != 3)
                    continue;
                if(dp[s1][s2][p1][p2] == true){
                    done = true;
                    cout << s1 << ":" << s2 << endl;
                    vector<pii> ans;
                    while(s1 + s2 + p1 + p2 != 0){
                        //cout << s1 << " " << s2 << " " << p1 << " " << p2 << endl;
                        ans.pb(mov[s1][s2][p1][p2]);
                        int x = ans.back().first;
                        int y = ans.back().second;

                        p1 -= x;
                        p2 -= y;
                        if(x > y)
                            s1 --;
                        else
                            s2 --;
                    }
                    reverse(ans.begin() , ans.end());
                    for(int i = 0 ; i < ans.size() ; i ++ )
                        cout << ans[i].first << ":" << ans[i].second << " ";
                    cout << endl;
                    break;
                }
            }
        }
        if(!done){
            cout << "Impossible" << endl;
        }
    }
    return 0;
}
