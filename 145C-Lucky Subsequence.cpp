

///link to problem : https://codeforces.com/problemset/problem/145/C



#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back

using namespace std;

typedef long long ll;
typedef pair<int , int> pii;
typedef long double ld;
typedef map<pii,set<pii> >::iterator mapit;
typedef set<int>::iterator setit;


const int maxn = 3e5 + 43;
const int maxm = 3003;
const ll maxii = 1e11 ;
const int maxlog = 20;
const ll mod = 1e9 + 7;
const int sq = 350;
const int inf = 2e9 + 43 ;
const ld PI  =3.141592653589793238463;
const ld eps = 1e-12;
ll dp[maxm][maxm];
ll fact[maxn];
map<int,int> cnt;
bool is_lucky(int x){
    while(x){
        int r = x % 10;
        if(r != 4 && r != 7)
            return false;
        x /= 10;
    }
    return true;
}
ll powmod(ll a , ll b){
    if(b == 0)
        return 1;
    ll ret = powmod(a , b / 2);
    ret *= ret;
    ret %= mod;
    if(b % 2 == 1)
        ret = (ret * a) % mod;
    return ret;
}
ll comb(int a , int b){
    if(b < 0)
        return 0;
    if(a < b)
        return 0;
    ll makhraj = fact[b] * fact[a - b];
    makhraj %= mod;

    return (fact[a] * powmod(makhraj , mod - 2)) % mod;
}
int main(){
    ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
    int n , k ;
    cin >> n >> k;
    fact[0] = 1;
    for(int i = 1 ; i < maxn ; i ++ )
        fact[i] = (fact[i - 1] * (ll)i)% mod;

    int c2 = 0;
    for(int i = 0 ; i < n ; i ++ ){
        int x ;
        cin >> x;
        if(is_lucky(x))
            cnt[x] ++ ;
        else
            c2 ++ ;
    }
    dp[0][0] = 1;
    int id = 0;
    for(map<int,int> :: iterator it = cnt.begin() ; it != cnt.end() ; it ++ ){
        id ++ ;
        int c = (*it).second;
        dp[id][0] = 1;
        for(int j = 1 ; j <= id ; j ++ ){
            dp[id][j] = dp[id - 1][j] + (ll) c * dp[id - 1][j - 1];
            dp[id][j] %= mod;
        }
    }
    ll ans = 0;
    for(int i = 0 ; i <= id ; i ++ ){
        ans += dp[id][i] * comb(c2 , k - i);
        ans %= mod;
    }
    cout << ans << endl;


	return 0;
}
